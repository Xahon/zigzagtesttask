﻿Shader "ZigZag/DisappearInFog" {
    Properties {
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
        _Glossiness ("Smoothness", Range(0,1)) = 0.5
        _Metallic ("Metallic", Range(0,1)) = 0.0

        _FogColor ("Fog Color", Color) = (1,1,1,1)
        _YHigh ("High Y", Float) = -1
        _YLow ("Low Y", Float) = -2
    }
    SubShader {
        Tags {
            "RenderType" = "Transparent"
            "Queue" = "Transparent"
        }
        LOD 200

        Pass {
            ColorMask 0
        }

        ZWrite On
        ZTest LEqual
        Blend SrcAlpha OneMinusSrcAlpha

        CGPROGRAM
        #pragma surface surf Standard alpha:fade
        #pragma target 3.0

        sampler2D _MainTex;

        struct Input {
            float3 worldPos;
            float2 uv_MainTex;
        };

        half _Glossiness;
        half _Metallic;
        fixed4 _Color;

        float _YHigh;
        float _YLow;
        fixed4 _FogColor;

        void surf (Input IN, inout SurfaceOutputStandard o) {
            fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;

            float fogStep = saturate(lerp(0, 1, smoothstep(_YHigh, _YLow, IN.worldPos.y)));
            o.Albedo =  (1 - fogStep) * c.rgb + fogStep * _FogColor.rgb * _FogColor.a;
            o.Alpha = 1 - fogStep;

            o.Metallic = _Metallic;
            o.Smoothness = _Glossiness;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
