﻿using ZigZag.Core;

namespace ZigZag.Controls {
    public interface IFallableDown : IResettable {
        bool isFalling { get; set; }
    }
}
