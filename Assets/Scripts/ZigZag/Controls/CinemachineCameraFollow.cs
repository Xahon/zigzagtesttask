﻿using Cinemachine;
using UnityEngine;
using ZigZag.Core;

namespace ZigZag.Controls {
    [RequireComponent(typeof(CinemachineVirtualCamera))]
    public class CinemachineCameraFollow : MonoBehaviour, ICameraFollow {
        CinemachineVirtualCamera m_vcam;

        void Awake() {
            m_vcam = GetComponent<CinemachineVirtualCamera>();
        }

        public void Follow(Transform target) {
            m_vcam.Follow = target;
        }

        public void Unfollow() {
            m_vcam.Follow = null;
        }
    }
}
