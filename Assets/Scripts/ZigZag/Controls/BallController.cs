﻿using System;
using UnityEngine;
using ZigZag.Core;

namespace ZigZag.Controls {
    [RequireComponent(typeof(SphereCollider))]
    public class BallController : MonoBehaviour, IBallController {
        [SerializeField]
        Transform m_ballDefaultPosition;

        [SerializeField]
        float m_moveSpeed;

        [SerializeField]
        float m_raycastRadiusCheckMult = 1;

        SphereCollider m_collider;
        readonly RaycastHit[] m_hitsCache = new RaycastHit[4];
        MovementDirection m_movementDirection;

        public event Action onFeltDown = delegate { };
        public bool isFallingDown { get; private set; }

        void Awake() {
            m_collider = GetComponent<SphereCollider>();
        }

        public void SwapDirection() {
            SetDirection(m_movementDirection == MovementDirection.forward ? MovementDirection.right : MovementDirection.forward);
        }

        public void SetDirection(MovementDirection movementDirection) {
            if (m_movementDirection == movementDirection) {
                return;
            }

            m_movementDirection = movementDirection;

            if (m_movementDirection == MovementDirection.forward) {
                transform.forward = Vector3.forward;
            }
            else {
                transform.forward = Vector3.right;
            }
        }

        void Update() {
            if (isFallingDown) {
                return;
            }

            int count = Physics.SphereCastNonAlloc(
                transform.position,
                m_collider.radius * m_raycastRadiusCheckMult,
                Vector3.down,
                m_hitsCache
            );
            bool grounded = count > 0;
            if (count == 1) {
                bool isSelf = m_hitsCache[0].collider.Equals(m_collider);
                grounded = !isSelf;
            }

            if (grounded) {
                transform.Translate(transform.forward * (m_moveSpeed * Time.deltaTime), Space.World);
            }
            else {
                FallDown();
            }
        }

        void FallDown() {
            if (isFallingDown)
                return;

            FreeFall freeFall = GetComponent<FreeFall>();
            if (freeFall) {
                freeFall.isFalling = true;
            }

            isFallingDown = true;
            onFeltDown.Invoke();
        }

        public void Reset() {
            transform.position = m_ballDefaultPosition.position;
            SetDirection(MovementDirection.forward);

            isFallingDown = false;
            GetComponent<FreeFall>()?.Reset();
        }
    }
}
