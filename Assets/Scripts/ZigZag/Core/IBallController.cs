﻿using System;

namespace ZigZag.Core {
    public interface IBallController : IResettable {
        event Action onFeltDown;

        bool isFallingDown { get; }
        void SwapDirection();
        void SetDirection(MovementDirection movementDirection);
    }
}
