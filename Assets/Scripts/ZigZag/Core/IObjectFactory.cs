﻿using UnityEngine;

namespace ZigZag.Core {
    public interface IObjectFactory {
        void DestroyObject(Object obj, float time = 0);
    }
}