﻿namespace ZigZag.Core {
    public interface IScoreManager : IResettable {
        int currentScore { get; }

        void Increase();
    }
}
