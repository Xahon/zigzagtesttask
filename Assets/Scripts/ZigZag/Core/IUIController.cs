﻿namespace ZigZag.Core {
    public interface IUIController {
        void SetState(UIState state);
        void SetScore(int score);
    }
}
