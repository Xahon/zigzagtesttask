﻿using UnityEngine;

namespace ZigZag.Core {
    interface ICameraFollow {
        void Follow(Transform target);
        void Unfollow();
    }
}
