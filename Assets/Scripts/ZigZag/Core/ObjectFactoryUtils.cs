﻿using UnityEngine;

namespace ZigZag.Core {
    public static class ObjectFactoryUtils {
        public static void PrintMismatchTypeError<T>(Object objectGiven) where T : Component {
            Debug.LogError($"Object factory: Mismatch type error. Expected {typeof(T).Name}, but {objectGiven.GetType().Name} given");
        }
    }
}