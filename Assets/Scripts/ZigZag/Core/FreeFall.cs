﻿using UnityEngine;
using ZigZag.Controls;

namespace ZigZag.Core {
    public class FreeFall : MonoBehaviour, IFallableDown {
        public bool isFalling { get; set; }
        Vector3 m_freeFallVelocity;

        public void Reset() {
            isFalling = false;
            m_freeFallVelocity = Vector3.zero;
        }

        void Update() {
            if (isFalling) {
                m_freeFallVelocity += Physics.gravity * Time.deltaTime;
                Vector3 movement = m_freeFallVelocity * Time.deltaTime;
                transform.Translate(movement);
            }
        }
    }
}
