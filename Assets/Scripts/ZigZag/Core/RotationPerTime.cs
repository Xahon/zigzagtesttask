﻿using UnityEngine;

namespace ZigZag.Core {
    public class RotationPerTime : MonoBehaviour {
        public float anglePerSecond = 30;

        void Update() {
            transform.RotateAround(transform.position, Vector3.up, anglePerSecond * Time.deltaTime);
        }
    }
}
