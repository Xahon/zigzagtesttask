﻿namespace ZigZag.Core {
    public interface IResettable {
        void Reset();
    }
}