﻿using System.Linq;
using UnityEngine;

namespace ZigZag.Core {
    public class GameController : MonoBehaviour {
        ICameraFollow m_cameraFollow;
        IBallController m_ballController;
        IUIController m_uiController;
        ILevelBuilder m_levelBuilder;
        IDiamondHooks m_diamondHooks;
        IScoreManager m_scoreManager;

        void Awake() {
            m_cameraFollow = FindObjectsOfType<MonoBehaviour>().OfType<ICameraFollow>().FirstOrDefault();
            m_ballController = FindObjectsOfType<MonoBehaviour>().OfType<IBallController>().FirstOrDefault();
            m_uiController = FindObjectsOfType<MonoBehaviour>().OfType<IUIController>().FirstOrDefault();
            m_levelBuilder = FindObjectsOfType<MonoBehaviour>().OfType<ILevelBuilder>().FirstOrDefault();
            m_diamondHooks = FindObjectsOfType<MonoBehaviour>().OfType<IDiamondHooks>().FirstOrDefault();
            m_scoreManager = FindObjectsOfType<MonoBehaviour>().OfType<IScoreManager>().FirstOrDefault();

            RestartGame();
        }

        public void RestartGame() {
            m_cameraFollow.Follow(((Component) m_ballController).transform);
            m_ballController.Reset();
            m_scoreManager.Reset();

            m_levelBuilder.SetAutoRebuild();

            m_ballController.onFeltDown -= OnBallFeltDown;
            m_ballController.onFeltDown += OnBallFeltDown;

            if (m_diamondHooks != null) {
                m_diamondHooks.onDiamondTaken -= OnDiamondTaken;

                if (m_scoreManager != null) {
                    m_diamondHooks.onDiamondTaken += OnDiamondTaken;
                }
            }

            if (m_uiController != null) {
                m_uiController.SetState(UIState.none);

                if (m_scoreManager != null) {
                    m_uiController.SetState(UIState.game);
                    m_uiController.SetScore(0);
                }
            }
        }

        void OnBallFeltDown() {
            m_cameraFollow.Unfollow();

            if (m_uiController != null) {
                m_uiController.SetState(UIState.lost);
            }
        }

        void OnDiamondTaken(Diamond diamond) {
            m_scoreManager.Increase();

            if (m_uiController != null) {
                m_uiController.SetScore(m_scoreManager.currentScore);
            }
        }
    }
}
