﻿using System.Linq;
using UnityEngine;

namespace ZigZag.Core {
    public class InputManager : MonoBehaviour {
        [SerializeField]
        GameController m_gameController;

        [Header("Debug")]
        [SerializeField]
        bool m_editorControllable;

        [SerializeField]
        MovementDirection m_direction;

        IBallController m_ballController;

        void Awake() {
            m_ballController = FindObjectsOfType<MonoBehaviour>().OfType<IBallController>().FirstOrDefault();
        }

        void Update() {
#if DEBUG
            if (m_editorControllable) {
                m_ballController.SetDirection(m_direction);
            }
#endif

            if (Input.GetMouseButtonDown(0) || Input.GetKeyDown(KeyCode.Space)) {
                if (!m_ballController.isFallingDown) {
                    m_ballController.SwapDirection();
                }
                else {
                    m_gameController.RestartGame();
                }
            }
        }
    }
}
