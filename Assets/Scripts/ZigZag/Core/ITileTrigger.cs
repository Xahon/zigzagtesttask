﻿using System;

namespace ZigZag.Core {
    public interface ITileTrigger {
        event Action onEnteredTile;
        event Action onExitTile;
    }
}