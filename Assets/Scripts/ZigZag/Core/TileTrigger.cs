﻿using System;
using UnityEngine;

namespace ZigZag.Core {
    [RequireComponent(typeof(Collider))]
    public class TileTrigger : MonoBehaviour, ITileTrigger {
        [SerializeField]
        LayerMask m_triggerableLayerMask;

        Collider m_collider;
        bool m_enteredTrigger;
        bool m_exitTrigger;
        float m_currentTime;

        public event Action onEnteredTile = delegate { };
        public event Action onExitTile = delegate { };

        void Awake() {
            m_collider = GetComponent<Collider>();
            m_collider.isTrigger = true;
        }

        void OnTriggerEnter(Collider other) {
            if (IsInLayer(other.gameObject)) {
                m_enteredTrigger = true;
                onEnteredTile.Invoke();
            }
        }

        void OnTriggerExit(Collider other) {
            if (m_enteredTrigger && IsInLayer(other.gameObject)) {
                m_exitTrigger = true;
                onExitTile.Invoke();
            }
        }

        bool IsInLayer(GameObject go) {
            int layer = go.layer;
            bool inLayer = (m_triggerableLayerMask.value & (1 << layer)) == m_triggerableLayerMask.value;
            return inLayer;
        }
    }
}
