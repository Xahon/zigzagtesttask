﻿using System.Collections;
using UnityEngine;

namespace ZigZag.Core {
    public class Diamond : MonoBehaviour {
        [SerializeField]
        AnimationCurve m_rotationChangeOnTaken, m_scaleChangeOnTaken;

        [SerializeField]
        float m_animationTime = 1;

        public float animationTime => m_animationTime;

        Coroutine m_animationCR;

        public void PlayTakenAnimation() {
            StopTakenAnimationCR();
            m_animationCR = StartCoroutine(PlayTakenAnimationCR());
        }

        void StopTakenAnimationCR() {
            if (m_animationCR != null) {
                StopCoroutine(m_animationCR);
            }
            m_animationCR = null;
        }

        IEnumerator PlayTakenAnimationCR() {
            var rotationPerTime = GetComponent<RotationPerTime>();
            if (!rotationPerTime) {
                yield break;
            }

            float timePassed = 0;
            float baseRotationSpeed = rotationPerTime.anglePerSecond;
            Vector3 baseScale = transform.localScale;

            while (true) {
                timePassed += Time.deltaTime;
                float step = Mathf.Clamp01(timePassed / m_animationTime);

                rotationPerTime.anglePerSecond = baseRotationSpeed * m_rotationChangeOnTaken.Evaluate(step);
                transform.localScale = baseScale * m_scaleChangeOnTaken.Evaluate(step);

                if (timePassed <= 0) {
                    break;
                }
                yield return null;
            }

            rotationPerTime.anglePerSecond = baseRotationSpeed;
        }
    }
}
