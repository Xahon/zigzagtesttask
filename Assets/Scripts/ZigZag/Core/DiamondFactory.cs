﻿using UnityEngine;

namespace ZigZag.Core {
    public class DiamondFactory : MonoBehaviour, IObjectFactory {
        [SerializeField]
        Diamond m_diamondPrefab;

        public Diamond InstantiateObject(Tile tile) {
            Diamond diamond = Instantiate(m_diamondPrefab, tile.transform, true);
            diamond.name = "Diamond";
            return diamond;
        }

        public void DestroyObject(Diamond diamond, float time = 0) {
            diamond.PlayTakenAnimation();
            Destroy(diamond.gameObject, diamond.animationTime + time);
        }

        public new void DestroyObject(Object obj, float time = 0) {
            if (obj == null) {
                return;
            }

            if (obj is Diamond diamond) {
                DestroyObject(diamond);
            }
            else if (obj is GameObject go) {
                DestroyObject(go.GetComponent<Diamond>());
            }
            else {
                ObjectFactoryUtils.PrintMismatchTypeError<Diamond>(obj);
            }
        }
    }
}
