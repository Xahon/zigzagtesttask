﻿using UnityEngine;

namespace ZigZag.Core {
    public class TileFactory : MonoBehaviour, IObjectFactory {
        [SerializeField]
        Tile m_tilePrefab;

        int m_lastTileIndex;

        public Tile InstantiateObject(Transform parent) {
            Tile tile = Instantiate(m_tilePrefab, parent, true);
            tile.name = "Tile " + ++m_lastTileIndex;
            return tile;
        }

        public void DestroyObject(Tile diamond, float time = 0) {
            Destroy(diamond.gameObject, time);
        }


        public new void DestroyObject(Object obj, float time = 0) {
            if (obj == null) {
                return;
            }

            if (obj is Tile tile) {
                DestroyObject(tile);
            }
            else if (obj is GameObject go) {
                DestroyObject(go.GetComponent<Tile>());
            }
            else {
                ObjectFactoryUtils.PrintMismatchTypeError<Tile>(obj);
            }
        }
    }
}
