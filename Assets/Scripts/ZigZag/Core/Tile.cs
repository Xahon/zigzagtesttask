﻿using JetBrains.Annotations;
using UnityEngine;

namespace ZigZag.Core {
    public class Tile : MonoBehaviour, IResettable {
        [SerializeField, Range(0, 10)]
        float m_timeToStartFall;

        bool m_exitTrigger;
        float m_currentTime;
        ITileTrigger m_tileTrigger;

        [CanBeNull]
        public ITileTrigger tileTrigger => m_tileTrigger;

        public bool isFallingDown { get; private set; }

        void Awake() {
            m_tileTrigger = GetComponentInChildren<ITileTrigger>();
            if (m_tileTrigger != null) {
                m_tileTrigger.onExitTile += TileTriggerOnExitTile;
            }
        }

        void TileTriggerOnExitTile() {
            m_exitTrigger = true;
        }

        void Update() {
            if (!m_exitTrigger)
                return;

            m_currentTime += Time.deltaTime;
            if (m_currentTime >= m_timeToStartFall) {
                FallDown();
            }
        }

        void FallDown() {
            if (isFallingDown)
                return;

            FreeFall freeFall = GetComponent<FreeFall>();
            if (freeFall) {
                freeFall.isFalling = true;
            }
            isFallingDown = true;
        }

        public void Reset() {
            transform.position = Vector3.zero;
            m_exitTrigger = false;
            m_currentTime = 0;

            isFallingDown = false;
            GetComponent<FreeFall>()?.Reset();
        }
    }
}
