﻿using System;

namespace ZigZag.Core {
    public interface IDiamondHooks {
        event Action<Diamond> onDiamondTaken;
    }
}
