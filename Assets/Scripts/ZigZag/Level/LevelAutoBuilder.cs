﻿using UnityEngine;
using ZigZag.Core;

namespace ZigZag.Level {
    [RequireComponent(typeof(LevelBuilder))]
    public class LevelAutoBuilder : MonoBehaviour, ILevelBuilder {
        LevelBuilder m_levelBuilder;

        ITileTrigger m_lastTileTileTrigger;

        void Awake() {
            m_levelBuilder = GetComponent<LevelBuilder>();
        }

        public void RebuildLevelAuto() {
            m_levelBuilder.DestroyChunks();

            // Build 2 chunks, then place a trigger in the middle of them, then create another chunk to remove seams
            m_levelBuilder.BuildNextChunk();
            m_levelBuilder.BuildNextChunk();
            SetAutoBuildCheckPoint();
            m_levelBuilder.BuildNextChunk();
        }

        void SetAutoBuildCheckPoint() {
            Tile tile = m_levelBuilder.tiles.Last.Value;
            m_lastTileTileTrigger = tile.tileTrigger;
            if (m_lastTileTileTrigger == null) {
                Debug.LogError($"Couldn't set level auto generation, because tile doesn't have {nameof(ITileTrigger)} component", tile);
                return;
            }
            m_lastTileTileTrigger.onEnteredTile += OnCheckpointTileEntered;
        }

        void OnCheckpointTileEntered() {
            print("Level: auto build");
            m_lastTileTileTrigger.onEnteredTile -= OnCheckpointTileEntered;
            SetAutoBuildCheckPoint();
            m_levelBuilder.ReuseFirstChunk();
        }

        public void SetAutoRebuild() {
            RebuildLevelAuto();
        }
    }
}
