﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using ZigZag.Core;
using Random = UnityEngine.Random;

namespace ZigZag.Level {
    public class DiamondGenerator : MonoBehaviour, IDiamondHooks {
        [SerializeField]
        LevelBuilder m_levelBuilder;

        [SerializeField, Range(0, 1)]
        float m_spawnRate = 0.03f;

        DiamondFactory m_diamondFactory;

        public event Action<Diamond> onDiamondTaken = delegate { };

        void Awake() {
            m_diamondFactory = FindObjectsOfType<MonoBehaviour>().OfType<DiamondFactory>().FirstOrDefault();
            m_levelBuilder.onNewChunk += OnNewChunk;
        }

        void OnNewChunk(IEnumerable<Tile> tiles) {
            if (m_diamondFactory == null) {
                Debug.LogError($"Couldn't generate diamonds, because there is no {nameof(DiamondFactory)} in scene", this);
                return;
            }

            foreach (Tile tile in tiles) {
                bool hasDiamond = Random.Range(0f, 1f) <= m_spawnRate;
                if (hasDiamond) {
                    SetTileDiamond(tile);
                }
            }
        }

        void SetTileDiamond(Tile tile) {
            if (tile.tileTrigger == null) {
                Debug.LogError($"Couldn't generate diamond, because tile doesn't have {nameof(ITileTrigger)} component", tile);
                return;
            }

            Diamond diamond = m_diamondFactory.InstantiateObject(tile);
            diamond.transform.localPosition = Vector3.up;

            void OnTakeDiamond() {
                if (tile.tileTrigger != null) {
                    tile.tileTrigger.onEnteredTile -= OnTakeDiamond;
                }
                onDiamondTaken.Invoke(diamond);
                m_diamondFactory.DestroyObject(diamond.gameObject);
            }

            tile.tileTrigger.onEnteredTile += OnTakeDiamond;
        }
    }
}
