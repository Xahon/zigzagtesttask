﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using ZigZag.Core;
using Random = UnityEngine.Random;

namespace ZigZag.Level {
    public class LevelBuilder : MonoBehaviour {
        [SerializeField]
        int m_chunkSize = 10;

        [SerializeField, Header("Debug")]
        bool m_flatTiles;

        TileFactory m_tileFactory;
        readonly LinkedList<Tile> m_tiles = new LinkedList<Tile>();
        int m_lastTileIndex;

        public LinkedList<Tile> tiles => m_tiles;
        public event Action<IEnumerable<Tile>> onNewChunk = delegate { };

        void Awake() {
            m_tileFactory = FindObjectsOfType<MonoBehaviour>().OfType<TileFactory>().FirstOrDefault();
        }

        public void DestroyChunks() {
            if (m_tileFactory == null) {
                Debug.LogError($"Couldn't destroy tile, because there is no {nameof(TileFactory)} in scene", this);
                return;
            }

            foreach (Tile tile in m_tiles) {
                if (!tile) {
                    continue;
                }

                if (Application.isPlaying) {
                    m_tileFactory.DestroyObject(tile);
                }
                else {
                    DestroyImmediate(tile.gameObject);
                }
            }
            m_tiles.Clear();
        }

        public void BuildNextChunk() {
            if (m_tileFactory == null) {
                Debug.LogError($"Couldn't generate tiles, because there is no {nameof(TileFactory)} in scene", this);
                return;
            }

            for (int i = 0; i < m_chunkSize; ++i) {
                var tile = m_tileFactory.InstantiateObject(transform);
                InitTileState(tile);
                m_tiles.AddLast(tile);
            }

            FireChunkUpdateEvent();
        }

        public void ReuseFirstChunk() {
            LinkedListNode<Tile> lastInChunk = m_tiles.First;
            for (int i = 0; i < m_chunkSize; i++) {
                lastInChunk = lastInChunk?.Next;
            }
            if (lastInChunk == null) {
                Debug.Log("Cannot reuse chunk. Building new one");
                BuildNextChunk();
                return;
            }

            for (int i = 0; i < m_chunkSize; i++) {
                LinkedListNode<Tile> tileNode = m_tiles.First;
                tileNode.Value.Reset();
                InitTileState(tileNode.Value);

                m_tiles.RemoveFirst();
                m_tiles.AddLast(tileNode);
            }
            Debug.Log($"Reused {m_chunkSize} tiles");

            FireChunkUpdateEvent();
        }

        void FireChunkUpdateEvent() {
            onNewChunk.Invoke(m_tiles.Skip(m_tiles.Count - m_chunkSize));
        }

        void InitTileState(Tile tile) {
            SetTileNextState(tile);
        }

        void SetTileNextState(Tile tile) {
            Vector3 position = Vector3.zero;
            if (m_tiles.Count > 0) {
                position = m_tiles.Last.Value.transform.position;

                if (m_flatTiles) {
                    position.z += 1;
                }
                else {
                    int direction =
                        m_tiles.Count == 1 // Second tile - always the same direction
                            ? 0
                            : Random.Range(0, 2);

                    // If direction == 0 - then increase z coordinate
                    // If direction == 1 - then increase x coordinate

                    if (direction == 0)
                        position.z += 1;
                    else
                        position.x += 1;
                }
            }
            tile.transform.position = position;
        }


#if UNITY_EDITOR
        [UnityEditor.CustomEditor(typeof(LevelBuilder))]
        class LevelBuilderEditor : UnityEditor.Editor {
            LevelBuilder t => (LevelBuilder) target;

            public override void OnInspectorGUI() {
                base.OnInspectorGUI();

                if (GUILayout.Button("Clear")) {
                    t.DestroyChunks();

                    List<Transform> children = t.transform.Cast<Transform>().ToList();
                    for (int i = 0; i < children.Count; i++) {
                        DestroyImmediate(children[i].gameObject);
                    }
                }

                if (GUILayout.Button("Generate level (Clear)")) {
                    t.DestroyChunks();
                    t.BuildNextChunk();
                }

                if (GUILayout.Button("Generate level (Next chunk)")) {
                    t.BuildNextChunk();
                }
            }
        }
#endif
    }
}
