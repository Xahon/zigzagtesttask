﻿using UnityEngine;
using ZigZag.Core;

namespace ZigZag.Score {
    public class ScoreManager : MonoBehaviour, IScoreManager {
        public int currentScore { get; private set; }

        public void Reset() {
            currentScore = 0;
        }

        public void Increase() {
            currentScore += 1;
        }
    }
}
