﻿using UnityEngine;
using ZigZag.Core;

namespace ZigZag.UI {
    public class UIController : MonoBehaviour, IUIController {
        [Header("Panels")]
        [SerializeField]
        GameObject m_panelLost, m_panelScore;

        [SerializeField]
        TMPFormatter m_textScore;

        UIState m_state;

        public void SetState(UIState state) {
            m_state = state;
            switch (state) {
                case UIState.none:
                    m_panelLost.SetActive(false);
                    m_panelScore.SetActive(false);
                    break;
                case UIState.game:
                    m_panelLost.SetActive(false);
                    m_panelScore.SetActive(true);
                    break;
                case UIState.lost:
                    m_panelLost.SetActive(true);
                    m_panelScore.SetActive(true);
                    break;
            }
        }

        public void SetScore(int score) {
            m_textScore.SetText(score);
        }
    }
}
