﻿using TMPro;
using UnityEngine;

namespace ZigZag.UI {
    [RequireComponent(typeof(TextMeshProUGUI))]
    public class TMPFormatter : MonoBehaviour {
        TextMeshProUGUI m_tmp;

        string m_format;

        void Awake() {
            m_tmp = GetComponent<TextMeshProUGUI>();
            m_format = m_tmp.text;
        }

        public void SetText(params object[] args) {
            m_tmp.text = string.Format(m_format, args);
        }
    }
}